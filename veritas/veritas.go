package veritas

import (
    "fmt"
    "gopkg.in/redis.v5"
    "os"
    "net"
)

func Register(default_name string, default_port string, redis_host string)(ret bool){
    myNAME := os.Getenv("APP_NAME")
    myPORT := os.Getenv("APP_PORT")
    
    //if not set by env-variable set default
	if myNAME == "" {
		myNAME = default_name
	}

    if myPORT == "" {
		myPORT = default_port
	}

	if redis_host == "" {
		redis_host = "localhost:6379"
	}


    myHOST := net.IPv4(127,0,0,1)
     
    host, _ := os.Hostname()
    addrs, _ := net.LookupIP(host)
    for _, addr := range addrs {
        if ipv4 := addr.To4(); ipv4 != nil {
            if(myHOST.IsLoopback()){
                myHOST = ipv4
            }
            fmt.Println("   found IPv4: ", ipv4)
        }   
    }

    fmt.Println("used myHost: ", myHOST)

    client := redis.NewClient(&redis.Options{
        Addr:     redis_host,
        Password: "", // no password set
        DB:       0,  // use default DB
    })

    //--- SET my Hostname (IP)
    IPKEY := myNAME + "host"
    err := client.Set(IPKEY, myHOST.String(), 0).Err()
    if err != nil {
        panic(err)
    }

    //--- SET my Port 
    PORTKEY := myNAME + "port"
    err = client.Set(PORTKEY, myPORT, 0).Err()
    if err != nil {
        panic(err)
    }

    return true
}

func Search(ServiceName string, redis_host string)(socket string){

	if redis_host == "" {
		redis_host = "localhost:6379"
	}

    client := redis.NewClient(&redis.Options{
        Addr:     redis_host,
        Password: "", // no password set
        DB:       0,  // use default DB
    })

    KEY := ServiceName + "host"
    host, err := client.Get(KEY).Result()
    if err != nil {
        panic(err)
    }

    KEY = ServiceName + "port"
    port, err := client.Get(KEY).Result()
    if err != nil {
        panic(err)
    }

    return host + ":" + port

}